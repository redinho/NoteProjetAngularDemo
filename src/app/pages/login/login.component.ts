import { Component, OnInit, NgModule, Input } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormsModule, ReactiveFormsModule, Validators, FormControl } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { UserService } from 'src/app/core/services/user/user.service';
import { User } from 'src/models/user.model';



@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  userConnected: User;
  password: FormControl = new FormControl('value', Validators.required);
  username: FormControl = new FormControl('value', Validators.required);
  
  constructor(private formBuilder: FormBuilder, private router: Router, private userService: UserService) { }

  ngOnInit(): void {
  }

  public openScreenAcceuil() {
    
    const username: string = this.username.value;
    const password: string = this.password.value;

    if (!username || !password) {
      return;
    }

    this.userService.getUser(username, password).subscribe(data => {

           if (null != data) {
              this.userConnected = data;
              this.router.navigate(['/acceuil']);
              console.log(this.userConnected.email);
          }
    });
  }

}
