import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Validators, FormControl, FormBuilder } from '@angular/forms';
import { Film } from 'src/models/film.model';
import { FilmService } from 'src/app/core/services/film/film.service';
import { Observable, of } from 'rxjs';
import { debounceTime, tap, catchError, distinctUntilChanged, switchMap } from 'rxjs/operators';
//import { NgbModule, NgbTypeahead, NgbTypeaheadModule, NgbTypeaheadSelectItemEvent, NgbTypeaheadConfig } from '@ng-bootstrap/ng-bootstrap';
//import { BrowserModule } from '@angular/platform-browser';


@Component({
  selector: 'app-acceuil',
  templateUrl: './acceuil.component.html',
  styleUrls: ['./acceuil.component.css']
})
export class AcceuilComponent implements OnInit {

  film: Film;
  title: FormControl = new FormControl('value', Validators.required);
  searchFailed = false;
  public model: any;
  term;
  searching = false;

  formatMatches = (value: any) => this.searchFailed ? this.term : value.title;
  search = (text$: Observable<string>) =>
    text$.pipe(
      debounceTime(300),
      distinctUntilChanged(),
      tap(() => this.searching = true),
      switchMap(term => {
        if (term.length > 2) {
          return this.filmService.getFilm(term).pipe(
            tap((reponse) => {
              if(reponse) {
                this.searchFailed = false;
              } else {
                throw new Error('Aucun résultat');
              }
            }),
            catchError((error) => {
              this.searchFailed = true;
              return of(['Aucun résultat']);
            }))
        }
      }
      ),
        tap(() => this.searching = false)
    )

  constructor(private formBuilder: FormBuilder, private router: Router, private filmService: FilmService) { }

  ngOnInit(): void {
  }

  

  rechercheFilm() {
    const title: string = this.title.value;
    this.filmService.getFilm(title).subscribe(data => {
           if (null != data) {
              this.film = data;
              console.log(this.film.releaseYear);
          }
    });
  }

  detailFilm(film) {
    if(!this.searchFailed) {
      const idFilm = film.idFilm;
      this.router.navigate(['/detail-film', idFilm]);
    }
  }
}
