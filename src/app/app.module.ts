import { BrowserModule } from '@angular/platform-browser';
import { LayoutModule } from '@angular/cdk/layout';
import { RouterModule, Routes } from '@angular/router';
import {HttpClientModule} from '@angular/common/http';
import {NgbModule, NgbTypeahead, NgbTypeaheadModule, NgbDateParserFormatter, NgbDatepickerI18n} from '@ng-bootstrap/ng-bootstrap';
//import { DashboardComponent } from './pages/dashboard/dashboard.component';
//import { MatieresComponent } from './pages/matieres/matieres.component';
import {MatiereService} from "./pages/matieres/matieres.service";
//import { AjoutMatiereComponent } from './pages/ajout-matiere/ajout-matiere.component';
import {ToastrModule} from "ngx-toastr";
import {ConfirmationPopoverModule} from 'angular-confirmation-popover';
import {routes} from './app-routing.module';

/* Routing */
import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';

/* Angular Material */
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AngularMaterialModule } from './angular-material.module';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

/* FormsModule */
import { FormsModule, FormGroup, ReactiveFormsModule } from '@angular/forms';

/* Angular Flex Layout */
import { FlexLayoutModule } from '@angular/flex-layout';

/* Components */
import { LoginComponent } from './pages/login/login.component';
import { RegisterComponent } from './pages/register/register.component';
import { AcceuilComponent } from './pages/acceuil/acceuil.component';
import { CommonModule } from '@angular/common';


/* const appRoutes: Routes = [
  { path: 'matiere', component: MatieresComponent },
  { path: 'ajout-matiere', component: AjoutMatiereComponent },
]; */

@NgModule({
  imports: [
    AngularMaterialModule,
    NgbModule,
    AppRoutingModule,
    FlexLayoutModule,
    BrowserModule,
    LayoutModule,
    BrowserAnimationsModule,
    HttpClientModule,
    RouterModule.forRoot(routes),
    ToastrModule.forRoot(),
    FormsModule,
    //FormGroup,
    ReactiveFormsModule,
    CommonModule,
    AngularMaterialModule,
    ConfirmationPopoverModule.forRoot({
      confirmButtonType: 'danger' // set defaults here
    })
  ],
  declarations: [
    AppComponent,
    //DashboardComponent,
    //MatieresComponent,
    //AjoutMatiereComponent,
    LoginComponent,
    RegisterComponent,
    AcceuilComponent
  ],
  providers: [
    MatiereService
  ],
  bootstrap: [AppComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AppModule { }
