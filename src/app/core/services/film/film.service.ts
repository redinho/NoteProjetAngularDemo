import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class FilmService {

   private urlApi = environment.urlApiRest;

   private urlfilm = this.urlApi + '/film/save';

   constructor(private http: HttpClient) {}

   getFilmList(): Observable<any> {
    return this.http.get('$(this.url}/film/list');
   }

   getFilm(title:string): Observable<any> {
    return this.http.get('$(this.url}/film/${title}');
   }

   createFilm(film:string): Observable<any> {
     return this.http.post('$(this.urlfilm}', film);
   }



}
