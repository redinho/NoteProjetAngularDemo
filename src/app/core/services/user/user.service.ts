import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Observable, of, throwError } from 'rxjs';
import { catchError } from 'rxjs/internal/operators/catchError';
import { HttpClient } from '@angular/common/http';
import { UserModel } from '../../interfaces/user-model';

@Injectable({
  providedIn: 'root'
})
export class UserService {

   private urlApi = environment.urlApiRest;

   constructor(private http: HttpClient) {}

  public getUser(username: string, password: string): Observable<any> {
 
    return this.http.get<UserModel>('http://localhost:8088/user', {
      params: {
        username,
        password
      }
    }).pipe(catchError(error => {
        return error;
      }));
   }

   createUser(user: UserModel): Observable<any> {
     return this.http.post('$(this.urlUser}', user);
   }
}
