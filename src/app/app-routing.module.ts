import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MatieresComponent } from './pages/matieres/matieres.component';
import { AjoutMatiereComponent } from './pages/ajout-matiere/ajout-matiere.component';
import { LoginComponent } from './pages/login/login.component';
import { RegisterComponent } from './pages/register/register.component';
import { AcceuilComponent } from './pages/acceuil/acceuil.component';

export const routes: Routes = [

  // tslint:disable-next-line: indent
  
     {
        path: '',
        redirectTo: 'acceuil',
        pathMatch: 'full'
    },

    {
      path: 'login',
      component: LoginComponent
    },

    { path: 'register',
      component: RegisterComponent
    },

    { path: 'acceuil',
      component: AcceuilComponent
    }

	  // {
    //     path: 'matieres',
    //     component: MatieresComponent
    // },
    // {
    //     path: 'ajout-matiere',
    //     component: AjoutMatiereComponent
    // }
 
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule, routes]
})
export class AppRoutingModule { }
