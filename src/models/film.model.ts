import { Inventory } from './inventory.model';
import { Category } from './category.model';
import { Language } from './language.model';

export class Film {

	title: string;

	description: string;

	releaseYear: number;

	language: Language;

	originalLanguagId: number;

	rentalDuration: number;

	rentalRate: number;

	length: number;

	replacementCost: number;

	rating: string;

	specialFeatures: string;

	actors: [];

	category: Category;

	inventory: Inventory;

	image: [];

}
